//
//  ContentView.swift
//  SimulasiTeam8
//
//  Created by Angelica Roseanne on 03/06/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Ahoy!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
